# Exemplo de telemetria para Datadog

Projeto que mostra como enviar traces para o Datadog do Lambda AWS, utilizando Open Telemetry.

Neste exemplo será utilizado Python, porém qualquer linguagem suportada pelo open telemetry pode ser utilizada

## Requisitos

* [AWS distro for opentelemetry](https://aws-otel.github.io/docs/introduction)
* Python3
* [Serverless Framework](https://www.serverless.com)
* Datadog
* [Open Telemetry](https://opentelemetry.io/docs/instrumentation/)

### Arquitetura

![arquitetura](assets/arquitetura.png)

A arquitetura tres componentes importantes

1. Open telemetry - para instrumentar a aplicação para geração de traces
2. AWS distro for opentelemetry - distribuição do Open Telemetry feita pela AWS que tem integração nativa com Lambda
3. AWS X-Ray - tem integração nativa como exporter para o Collector do AWS distro
4. Datadog - ferramenta de observabilidade

### Instrumentar o APP

Para instrumentar no site do Open Telemetry tem uma documentação ampla de como fazer, aqui vou focar no código deste app

* config.py

Possui todas as configurações para integrar com o exporter do open telemetry, olhar metodo **init_tracer**

* handler.py

Cria o span para instrumentar o App

```python
# Cria integração
import config
tracer = config.init_tracer('telemetry-sls')

# Starta span
with tracer.start_as_current_span("telemetry") as span:
    span.set_attribute("teste", '123')
```

### Como integrar o datadog

Para integrar com o Datadog é necessário setar o [AWS integration](https://www.datadoghq.com/blog/aws-1-click-integration/)

Após essa configuração é necessário configurar o X-Ray collector conforme imagem abaixo

![x-ray](assets/integration.png)

### Deploy da função

Para deploy é utilizado o serverless framework, que utiliza o arquivo **serverless.yml**

```bash
serverless deploy
```
