#! /usr/bin/python python3
import logging
from opentelemetry import trace
from opentelemetry.sdk.resources import SERVICE_NAME, Resource
from opentelemetry.sdk.trace import TracerProvider
import os
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor
)

logger = logging.getLogger(__name__)


def init_tracer(service_name):
    logger.debug('Instantiating the agent')
    resource = Resource(attributes={
        SERVICE_NAME: service_name
    })
    # Utiliza o endpoint como variavel de ambiente
    exporter = OTLPSpanExporter(endpoint=os.getenv("OTEL_EXPORTER_OTLP_ENDPOINT"))
    provider = TracerProvider(resource=resource)
    processor = BatchSpanProcessor(exporter)
    provider.add_span_processor(processor)

    trace.set_tracer_provider(provider)

    tracer = trace.get_tracer(__name__)

    return tracer


class TracerContext():
    """
    O contexto é importante para zerar o carrier em ambientes que estarão recebendo multiplas requisições

    Reinicializar o contexto

    .. code-block:: python

        import apm.tracer.tracer_config as conf
        # Reinicializa o contexto zerando carrier para a thread em questão
        conf.TracerContext()

    """
    __shared_instance = {}

    def __init__(self):
        self.__dict__ = self.__shared_instance
        self.carrier = {}
