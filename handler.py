import json
import logging.config
import os
import config

module_path = os.path.dirname(__file__)
default_path = module_path + "/log_config.cfg"
default_level = logging.INFO
env_key = "LOG_CFG"
path = default_path
value = os.getenv(env_key, None)
if value:
    path = value
if os.path.exists(path):
    logging.config.fileConfig(fname=path, disable_existing_loggers=False)
else:
    logging.basicConfig(level=default_level)

logger = logging.getLogger(__name__)
tracer = config.init_tracer('telemetry-sls')


def telemetry(event, context):
    with tracer.start_as_current_span("telemetry") as span:
        span.set_attribute("teste", '123')
    return {"statusCode": 200, "body": "sucesso"}
